<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?php print check_plain($node->title); ?></title>
    <!-- link rel="stylesheet" media="screen,projection,print" href="/files/Slidy/w3c-blue.css" type="text/css">
    <script type="text/javascript" src="/files/Slidy/slidy.js"></script -->
    
    <link rel="stylesheet" href="<?php print base_path()._peptalk_path('peptalk.css'); ?>" type="text/css">
    <script type="text/javascript" src="<?php print base_path().'misc/jquery.js'; ?>"></script>
    <script type="text/javascript">exitUrl = "<?php print base_path().'node/'.$node->nid;?>";</script>
    <script type="text/javascript" src="<?php print base_path()._peptalk_path('peptalk.js'); ?>"></script>
  </head>
  <body>
    <?php print $node->body; ?>
  </body>
</html>
