
/*
TODO:
optional front page with title and author
optional table of contents - with level to go down to (e.g. <h2>)
slide styles/templates
allow references to slides by #N or Slidy-style #(N) in browser URL field
update browser URL field ?
reveal incrementally (as in Slidy)
optional unfoldable/outlined content (as in Slidy)
optionally divide slides into sections for long presentations
  - optionally set <h1> .. <h5> to be slide section marker? or use divs?
  - could be multi-level?
allow wrapping divs around multiple slides to annotate for special treatment
  - div.slide creation must propagate annotation to each div.slide
"driver" modules for e.g. keyboard, Targus wireless controller
optional scrollbar / presentation player for navigating slides
optional resources slide with list of URLs in presentation
optional separate presenter-only window, showing next slide, current slide notes
printouts mode (incl. notes) and slideshow mode
simple image build effects (e.g. image fadeIn, slide in)
complex SVG build effects (e.g. piece by piece build)
one day, when we have decent SVG support in all browsers: lay out theme with SVG and insert HTML into its DOM
change internally-used div classes to "peptalk-" namespace
optional webcast mode - automatically moves to next slide on signal from server controlled by author
  (ajax getEvents :: myLastProcessedEventNumber -> array of events, where event e.g. moveToSlide(N))
webcast mode to allow recording of events to replay a webcast once over
webcast mode to allow attachment of voice stream to synchronize against (tricky)
use Drupal theme to set (overridable) default style for presentation (hard to get right)
make slides into proper objects (nextSlide(), previousSlide(), nextFrame() etc)
build effect - highlight current element (e.g. <li>)
proper navigation keys, help and feedback
audit code for possible cross-site scripting vulnerabilities, see http://drupal.org/node/28984
*/

slides = [];

$(function(){
  /* from Slidy: disable IE image popup */
  $("img").attr("galleryimg", "no");

  /* create slide and heading divs around headings */
  //  body.find("h1,h2,h3,h4,h5,h6").wrap("<div class='slide'><div class='slide-header'>"); FIXME why doesn't this work?
  $("h1,h2,h3,h4,h5,h6", $("body")).wrap("<div class='slide'><div class='slide-header'>");

  /* build slide array, moving any content after each slide into the slide body */
  var currentSlideBody;
  $("body").children().each(
    function(index){
      var element = $(this);
      if (element.is("div.slide")) {
        var slideNumber = slides.length;
        currentSlideBody = $("<div class='slide-body'>").appendTo(element);
        slides.push(element);
      } else {
        currentSlideBody.append(element);
      }
    }
  );
  
  showSlide(0);
});

function showSlide(slideNumber) {
  if (slideNumber >= slides.length || slideNumber < 0) {
    exitSlideshow();
  } else {
    $("body").children().remove();
    var slide = slides[slideNumber].clone(true);
    $("body").append(slide);
    slide.click(function(){showSlide(slideNumber+1);});
    // some jQuery functions only seem to work when object is part of document tree:
    slide.find(".notes-only").hide();
    slide.find(".slides-only").show();
  }
}

function showNotes() {
  $("body").children().remove();
  for each(var slide in slides) {
    alert(slide);
    $("body").append(slide);
  }
  $("body > .notes-only").show();
  $("body > .slides-only").hide();
}

function exitSlideshow() {
  location.href = exitUrl;
}
